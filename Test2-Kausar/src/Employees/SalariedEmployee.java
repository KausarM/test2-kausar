package Employees;

/**
 * An class that implements the Employee interface
 * @author Kausar 
 *
 */
public class SalariedEmployee implements Employee {

	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly=yearly;
	}
	
	@Override
	public double getYearlyPay() {
		return this.yearly;
		
	}
	
	
	
}
