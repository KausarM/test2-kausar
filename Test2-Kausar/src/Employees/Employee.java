package Employees;
/**
 * An Interface employee that will need to be implemented by other classes
 * @author Kausar 
 *
 */

public interface Employee {
	
	public double getYearlyPay();
}
