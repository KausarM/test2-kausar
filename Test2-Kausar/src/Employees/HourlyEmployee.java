package Employees;

/**
 * An class that implements the Employee interface
 * @author Kausar 
 *
 */
public class HourlyEmployee implements Employee{

	private int hours;
	private double hourlyPay;
	
	//getters
	
	public int getHours() {
		return this.hours;
	}
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	
	//constructor
	public HourlyEmployee (int hours, double hourlyPay) {
		this.hours=hours;
		this.hourlyPay=hourlyPay;
	}
	@Override
	public double getYearlyPay(){
		double total = this.hours * this.hourlyPay * 52;
		return total;
		
		
	}
}
