package Employees;

/**
 * Main method that creates an array of Employee and gets the total sum of every employee's salary 
 * @author Kausar 
 *
 */
public class PayrollManagement {
	
	public static double getTotalExpenses ( Employee [] input ) {
		
		double total = 0;
		
		for ( int i=0; i < input.length; i++) {
			total+= input[i].getYearlyPay();	
		}
		return total;
	}
	
	public static void main(String[] args) {
		Employee [] work = new Employee [5];
		work[0]= new SalariedEmployee (50000);
		work[1]= new HourlyEmployee (40,18);
		work[2]= new HourlyEmployee (37,19);
		work[3]= new UnionizedHourlyEmployee (37,20, 4000);
		work[4]= new UnionizedHourlyEmployee (45,15, 3000);
		
		//to get a break down of every employee's total yearly salary just to make it better, it is not required but its better to validate total this way 
		for( int i= 0; i< work.length ; i++) {
			System.out.println( work[i].getYearlyPay());		
		}
		
		System.out.println("Total: " + getTotalExpenses(work));
	}

}
