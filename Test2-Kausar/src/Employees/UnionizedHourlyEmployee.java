package Employees;
/**
 * An class that implements the Employee interface
 * @author Kausar 
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {
	
	private double pensionContribution;
	
	public UnionizedHourlyEmployee(int hours, double hourlyPay, double pensionContribution) {
		super(hours, hourlyPay);
		this.pensionContribution= pensionContribution;
		
	}
	@Override
	public double getYearlyPay() {
		double total = (getHours() * getHourlyPay() * 52) + pensionContribution ;
		return total;
		
	}

}
