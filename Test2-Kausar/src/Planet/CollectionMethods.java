package Planet;


/**
 * A method that will only return planets that the value of order is 1,2 or 3
 * @author Kausar 
 *
 */
import java.util.Collection;
import java.util.HashSet;


public class CollectionMethods {
	
	public static Collection <Planet> getInnerPlanets ( Collection <Planet> planets){
		HashSet <Planet> set = new HashSet <Planet>() ;
		
		for(Planet s : planets) {
			if ( s.getOrder()==1 || s.getOrder()==2 || s.getOrder()==3 ) {
				set.add(s);
			}
		}
		
		return set;
		
		
	}
}
	
	//main method to test above method
	/* public static void main(String[] args) {
		HashSet <Planet> set = new HashSet <Planet>() ;
		Planet alpha = new Planet ("Solar", "Pluto", 2, 8.5);
		Planet beta = new Planet ("Galaxy", "Pluto", 3, 8.5);
		Planet delta = new Planet ("Solar", "Venus", 3, 9.5);
		Planet fox = new Planet ("Solar", "Earth", 1, 9.5);
		Planet zooloo = new Planet ("Solar", "Plato", 4, 10.5);
		set.add(alpha);
		set.add(delta);
		set.add(beta);
		set.add(fox);
		set.add(zooloo);
		
		HashSet <Planet> brandnew = (HashSet<Planet>) getInnerPlanets(set) ;
		
		 if(alpha.equals(beta)) {
			System.out.println("yess");
		}
		else {
			System.out.println("nooo");
		}
		
		
		for ( Planet test : brandnew ) {
			System.out.println(test.getName());
		} */


	


