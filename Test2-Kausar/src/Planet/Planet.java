package Planet;

/**
 * A class to store information about a planet
 * @author Daniel
 * @author Kausar , modified it by adding overriden equals and hashcode method
 *
 */
public class Planet implements Comparable <Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	@Override
	public boolean equals (Object o) {
		Planet alpha = (Planet) o;
		
		if (! (o instanceof Planet)) {
			return false;
		}
		if (this.name.equals(alpha.getName()) && this.order== alpha.getOrder()) {
			return true;
		}
		return false;
	}
	
	public int hashCode() {
		String combined = this.name + this.order;
		return combined.hashCode();		
	}
	
	public int compareTo (Planet s) {
		int nameComparison = this.getName().compareTo(s.getName());
		
		
		if ( nameComparison == 0 ) {
			int planetSystem = s.getPlanetarySystemName().compareTo(this.getPlanetarySystemName());
			return planetSystem;
		}
		return nameComparison;
	}
	
}
